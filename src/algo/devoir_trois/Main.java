package algo.devoir_trois;

public class Main {
    public static void main(String[] args) {

        int[] x = new int[10];
        QuestionUne.tousEgauxAZero(x, 1, 4);

        int[] a = new int[]{2, 28, 6, 94};
        QuestionUne.tousDistincts(a);

        x = new int[]{2, 28, 6, 94};
        QuestionUne.tousPairs(x);

        x = new int[]{2, -4, 1, 0};
        QuestionUne.tousInfOuEgADeux(x);

        x = new int[]{2, 5, 1, 9};
        QuestionUne.tousInfADix(x);

        x = new int[]{2, 4, 0, 9};
        QuestionUne.zeroExiste(x);

        x = new int[]{2, 4, 7, 9};
        QuestionUne.elemsTries(x);


        QuestionDeux.max(8, 2);
    }

    public static class QuestionUne {

        //@ normal_behavior
        //@ requires j < x.length && x != null && (\forall int k; k >= i && k <= j; x[k] == 0);
        //@ assignable \nothing;
        public static void tousEgauxAZero(int[] x, int i, int j)
        {

        }

        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ requires (\forall int i, j; 0 <= i && i < a.length && 0 <= j && j < a.length && i != j; a[i] != a[j]);
        //@ assignable \nothing;
        public static void tousDistincts(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ requires (\forall int i; 0 <= i && i < a.length; a[i] % 2 == 0);
        //@ assignable \nothing;
        public static void tousPairs(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null;
        //@ requires (\forall int i, j; 0 < i && i <= j && j <= a.length; a[i-1] <= 2);
        //@ assignable \nothing;
        public static void tousInfOuEgADeux(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null;
        //@ requires (\forall int i, j; 0 < i && i <= j && j <= a.length; a[i-1] < 10);
        //@ assignable \nothing;
        public static void tousInfADix(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null;
        //@ requires (\forall int i, j; 0 < i && i <= j && j <= a.length; (\exists int x; 0 <= x && x < a.length; a[x] == 0));
        //@ assignable \nothing;
        public static void zeroExiste(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null;
        //@ requires (\forall int i; 0 < i && i < a.length; a[i-1] < a[i]);
        //@ assignable \nothing;
        public static void elemsTries(int[] a)
        {

        }

        //@ public normal_behavior
        //@ requires a != null;
        //@ requires (\forall int i; 0 < i && i <= a.length; (\exists int x; x < Integer.MAX_VALUE; x <= a [i]));
        //@ assignable \nothing;
        public static void xMinDeA(int[] a)
        {

        }
    }

    public static class QuestionDeux {

        //@ public normal_behavior
        //@ ensures (\result == x ==> x >= y) || (\result == y ==> y > x);
        //@ assignable \nothing;
        public static double max(double x, double y)
        {
            if (x >= y) {
                return x;
            } else {
                return y;
            }
        }

        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ assignable \nothing;
        //@ ensures \result == (\exists int k; 0 <= k && k < a.length; a[k] == x);
        public static boolean contient(double[] a, double x)
        {
            int i = 0;

            //@ loop_invariant 0 <= i && i < a.length;
            //@ loop_invariant !(\exists int j; 0 <= j && j < a.length && i < j; a[j] == x);
            //@ decreasing a.length - i;
            while (i < a.length) {
                if (a[i] == x) {
                    return true;
                }
                i++;
            }

            return false;
        }

        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ assignable \nothing;
        //@ ensures (\forall int k; 0 <= k && k < a.length; \result >= a[k]);
        //@ ensures (\exists int k; 0 <= k && k < a.length; \result == a[k]);
        public static double max(double[] a)
        {
            double max = a[0];
            int i = 0;


            //@ loop_invariant 0 <= i && i < a.length;
            //@ loop_invariant (\forall int j; 0 <= j && j < a.length && j < i; max >= a[j]);
            //@ decreasing a.length - i;
            while (i < a.length){
                if (a[i] > max) {
                    max = a[i];
                }
                i++;
            }

            return max;
        }

        //@ public normal behavior
        //@ requires n >= 0 && n < 2;
        //@ assignable \nothing;
        //@ ensure \result == 1;
        //@ also
        //@ public normal behavior
        //@ assignable \nothing;
        //@ requires 2 <= n && n <= 100;
        //@ ensures \result == n * factorielle(n-1);
        //@ mesured_by n;
        public static /*@ pure @*/ int factorielle(int n)
        {
            if (n == 0) {
                return 1;
            }

            return n * factorielle(n-1);
        }

        //@ public normal behavior
        //@ requires n >= 0;
        //@ assignable \nothing;
        //@ ensures n >= \result * \result;
        //@ ensures n < \result+1 * \result+1;
        public static int intSqrt(int n)
        {
            return (int) Math.sqrt(n);
        }
    }


    public static class QuestionDix
    {

        //@ public normal behavior
        //@ requires a != null && a.length > 0;
        //@ requires n <= 0 && x <= 0;
        //@ assignable \nothing;
        //@ ensures \result == ((\num_of int i; 0 <= i && i < a.length; a[i] == x) == n)
        public boolean enLigne(int[] a, int n, int x)
        {
            int i = 0;
            int count = 0;

            //@ loop_invariant 0 <= i && i < a.length;
            //@ loop_invariant (\num_of int j; 0 <= j && j < a.length && j < i; a[i] == x);
            //@ decreasing a.length - i;
            while (i < a.length) {
                if (a[i] == x) {
                    count++;
                }
                i++;
            }

            return count == n;
        }
    }

    public static class QuestionDouze
    {

        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ requires l < 0 && l < a.length;
        //@ assignable \nothing;
        //@ ensures (\forall int k; 0 <= k && k < a.length; \result == \old(somme) + a[i])
        public int somme(int[] a, int l)
        {
            int somme = 0;
            int i = 0;

            //@ loop_invariant 0 <= i && i < a.length;
            //@ loop_invariant (\forall int j; 0 <= j && j < a.length && j < i; somme == \old(somme) + a[j])
            //@ decreasing a.length - i;
            while (i < l) {
                somme = somme + a[i];
                i++;
            }

            return somme;
        }
    }

    //@ public normal_behavior
    //@ requires n >= 0 && m >= 0;
    //@ assignable \nothing;
    //@ ensures \result == m + n;
    public static class QuestionTreize
    {
        public int somme(int n, int m)
        {
            int res = m;
            int i = 0;

            //@ loop_invariant 0 <= i && i < n;
            //@ loop_invariant (\forall int j; 0 <= j && j < n && j < i; res == m + j)
            //@ decreasing a.length - i;
            while (i < n) {
                res = res + 1;
                i++;
            }
            return  res;
        }
    }



    public static class QuestionQuinze
    {
        //@ public normal_behavior
        //@ requires a >= 0 && b >= 0;
        //@ ensures (\result == a ==> a > b) || (\result == b ==> b >= a);
        //@ assignable \nothing;
        public int max(int a, int b)
        {
            if (a > b) {
                return a;
            } else {
                return b;
            }
        }
    }

    public static class QuestionVingtDeux
    {
        //@ public normal_behavior
        //@ requires a != null && a.length > 0;
        //@ assignable \nothing;
        //@ ensures (\forall int k; 0 <= k && k < a.length-1; a[k] <= a[k+1]);
        //@ ensures \old(a.length) == a.length;
        //@ ensures (\forall int l; 0 <= l && l < a.length; (\exists int m; 0 <= m && a.length; a[m] == \old(a[l])));
        public void tri(int[] a)
        {
            boolean trie = true;

            //@ loop_invariant trie == false ==> !(\forall int ; 0 <= k && k < a.length-1; a[k] <= a[k+1])
            //@ && (\forall int l; 0 <= l && l < a.length; (\exists int m; 0 <= m && a.length; a[m] == \old(a[l])));
            do {
                trie = true;

                //@ loop_invariant i <= 0 && i < a.length-1;
                //@ loop_invariant (\forall int j; 0 <= j && j < a.length-1 && j < i; a[j] <= a[j+1]);
                //@ decreasing a.length - 1 - i
                for (int i = 0; i < a.length-1; i++) {
                    if (a[i] > a[i+1]) {
                        int tmp = a[i];
                        a[i] = a[i+1];
                        a[i+1] = tmp;
                        trie = false;
                    }
                }
            } while (!trie);
        }
    }
}
