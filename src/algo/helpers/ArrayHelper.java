package algo.helpers;

public class ArrayHelper {
    //@ normal_behavior
    //@ requires x != null;
    //@ assignable \nothing;
    public static void printArray(int[] x) {
        System.out.print("Array = {");

        //@ decreasing x.length - i;
        //@ loop_invariant 0 <= i && i <= x.length;
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i]);
            if (i < x.length -1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");
    }
}